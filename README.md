# My Calm Window Manager Build

My .cwmrc for the Calm Window Manager, a keyboard-driven floating window manager forked from EvilWM and is part of the base OpenBSD project. Credit goes to https://github.com/leahneukirchen/cwm for keeping the Calm Window Manager project alive.
